
class apps {
    /**
     * Funkcja do zmiany parametrów pól wg pol
     * @param {type} polecenie 
     * @returns {undefined}
     */
    static fields(polecenie = "zastąpWszystkie") {
        var t = [];
        switch (polecenie) {
            case "zastąpWszystkie":
                t = [1, 0, 0, 1];
                break;
            case "wyodrębnij":
                t = [1, 0, 0, 0];
                break;
            case "podstaw":
                t = [1, 1, 1, 0];
                break;
            default:
                t = [1, 1, 1, 1];
        }
        for (let i = 0; i < 4; i++) {
            document.getElementById("parametry").getElementsByTagName("textarea")[i].disabled = !t[i];
        }
    }
    /**
     * 
     * @param {type} inName
     * @param {type} value
     * @param {type} app
     * @param {type} destinationId
     * @param {type} formId
     * @returns {undefined}
     */
    static post(inName, value, app = "Kompozytor", destinationId = "output", formId = "formularz") {

        let sum = [];
        sum += { inName: value };
        for (let t in document.getElementById(formId).getElementsByTagName("input")) {
            if (t.type == "text") {
                sum += t;
            }
        }
        $.post("https://bujakowski.dev/_LandingPage/apps/" + app + "/" + app + ".php",
            sum,
            function(data, status) {
                document.getElementById(destinationId).innerHTML = (data || status);
            }



        );
    }
}

var treść = {
    
    OpisDziałalności: '<h2>Profi-MB<br>Oferuje rozwiązania, ułatwiające obróbkę tekstów.<h2/>' +
        '<p>Obecnie rozwijam dwa projekty:</p>' +
        '<p>Dura Lex - ułatwiający czytanie rozwlekłych przepisów polskiego prawa,</p>' +
        '<p>Data Wiz - dzięki niemu masowe przekształcenia powtażalnych fragmentów tekstu</p>' +
        '<p>są o tyle łatwe, o ile wiemy: co jest zbędne w określonych przypadkach, czego brakuje zarządzanemu tekstowi. </p>' +
        '<p>Sztandarową funkcją nie jes wstawianie, usuwanie, zastępowanie - to proste i, oczywiście, dostępne w aplikacji.</p>' +
        '<p>To, czym program przyciąga to wstawianie tekstów ze zmiennymi - t.j. mając w opisie rządany ciąg (n.p. P-MB) możemy rozwinąć w cały element strony lub cokolwiek innego: </p>' +
        '<p>("P-MB", "X ma potencjał", "X" )= "\&ltH1&gtP-MB ma potencjał\&lt/H1&gt" ! I tak przy każdym wystąpieniu frazy P-MB!</p>'
    ,
    DuraLex: "<h2>Dwa tabulatory pozwalają na jednoczesne śledzenie dwóch źródeł <a>(lewy tab.: stronaA-stronaB , prawy tab.: dokumentA/dokumentB i t.p.) .</a> </p>"+
        "<p>&nbsp;&nbsp;&nbsp;Wiele funkcji - m.in. lokalizowanie dokumentów po jego parametrach.</p>" +
        "Przykład: w wyrażeniu \"Niniejsze rozporządzenie było poprzedzone rozporządzeniem Ministra Kultury i Dziedzictwa Narodowego z dnia 6 sierpnia 2015 r." +
        "w sprawie typów szkół artystycznych publicznych i niepublicznych <b>" +
        "<a>    (Dz. U. poz. 1210)</a>" +
        "</b>, które traci moc z dniem 1 stycznia 2017 r." +
        "zgodnie z<a><b> art. 32 pkt 1 ustawy z dnia 23 czerwca 2016 r.</b></a> o zmianie ustawy o systemie oświaty oraz niektórych innych ustaw <b><a>(Dz. U." +
        "  poz. 1010 i 1985)\"</a>" +
            "</b>.  <b><br>&nbsp;&nbsp;Pogrubiebie</b> to powoływanie innych przepisów - dzięki narzędziu ISAP można owe treści uzyskać w szybkim tempie.<br>" +
            "<br>&nbsp;&nbsp;&nbsp;Za pomocą kolejnych narzędzi można :" +
        "<ul>" +
        "  <li>skomponować w jeden plik .TXT;" +
        "  <li>skopiować wybrane strony z kilku PDF w jeden;" +
        "  <li>utworzyć plik z notatkami;" +
        "  <li> i wiele innych."

        +
        "</ul>" +
        "  &nbsp;&nbsp;&nbsp;Można otwierać pliki PDF w formie surowego tekstu, pliki TXT, otwierać obsługiwane pliki przez URI i je zapisywać w oryginalnym rozszerzeniu." +
        "  Narzędzie WEB to prosta przeglądarka, która rozpoznaje obsługiwane pliki w hiperlinkach a zawartość otwiera w nowej zakładce."
    ,
    DataWiz: " Proste narzędzie, wkorzystujące t.zw. regularne wyrażenia, by znajdować i zmieniać treść w źródle." +
            "Narzędzie potrafi:<br>" +
            "podmienić wskazaną część tekstu na nową (w tym na \"\" -t.j. skasować),<br>" +
            "wyciąć konkretną treść, podstawić ją pod wzór, wstawić podstawiony wzór w miejsce wycięcia,<br>" +
            "wstawić nową treść w miejsce przed/za wybranym tekstem, uwzględniając przesunięcie (l. całkowita) względem początku/końca wybranego tekstu.<br>" +
            "<br>" +
        "Posiada trzy duze pola t" +
            "podmienić wskazaną część tekstu na nową (w tym na \"\" -t.j. skasować),<br>" +
            "wyciąć konkretną treść, podstawić ją pod wzór, wstawić podstawiony wzór w miejsce wycięcia,<br>" +
            "wstawić nową treść w miejsce przed/za wybranym tekstem, uwzględniając przesunięcie (l. całkowita) względem początku/końca wybranego tekstu.<br>" +
            "<br>" +
            "Posiada trzy duze pola tekstowe - komunikaty, tekst źródłowy, tekst wyjściowy.<br>" +
            "Posiada funkcję cofnij/ponów ( skrót [CTRL]+Z, [CTRL]+Y) oraz zamiany wejścia z wyjściem.<br>" +
            "Możliwość zapisania sesji (parametry , źródło ,wyjście) i odtworzenia.<br>"
    ,
    Kompozytor: "Edytor WYSIWYG (ang. What You See Is What You Get)" +
            "Po zakończeniu edycji możesz skopiować kod do schowka, więc również wdrożyć zmianę w docelowym dokumencie." +
            "Planowane urozmaicenia:<br>" +
        "<ul>" +
        "<li> wdrożenie globalnych zmian z użyciem t.zw. regularnych wyrażeń;" +
        "<li> możliwość zapisywania swojej pracy na serwerze;" +
        "<li> możliwość zaimplementowania edytora we własnym panelu administracyjnym;" +
        "<li> możliwość ładowania kodu strony, wskazanej w linku URL" +
        "<li> nowy wygląd" +
        "<li> i wiele innych!" +
        "</ul>"
};

function pokażOpis( opis,  dest = "strona") {
    while(!document.getElementById(dest)){
	console.log('dest unready');};
	document.getElementById(dest).innerHTML = treść[opis];
}

function cytatDnia() {
/**
    //$().
    var l = document.createElement("div");
    var t = document.createTextNode('Wtedy powtórnie wszedł Piłat do pretorium, a przywoławszy Jezusa rzekł do Niego: "Czy Ty jesteś Królem Żydowskim?" Jezus odpowiedział: "Czy to mówisz od siebie, czy też inni powiedzieli ci o Mnie?" Piłat odparł: "Czy ja jestem Żydem? Naród Twój i arcykapłani wydali mi Ciebie. Coś uczynił?" Odpowiedział Jezus: "Królestwo moje nie jest z tego świata. Gdyby królestwo moje było z tego świata, słudzy moi biliby się, abym nie został wydany Żydom. Teraz zaś królestwo moje nie jest stąd". Piłat zatem powiedział\n\ndo Niego: "A więc jesteś królem?" Odpowiedział Jezus: "Tak, jestem królem. Ja się na to narodziłem i na to przyszedłem na świat, aby dać świadectwo prawdzie. Każdy, kto jest z prawdy, słucha mojego głosu.');
    l.id = "cytat";
    //l.style="position:absolute; left:0em;top:0em;width:10em;height:10em;";
    l.appendChild(t);
    $("#ważne").appendTo("#strona");
    //document.getElementById("strona").appendChild();*/
}

/**
 * Translates 2nd dimension arrays to rows, where null values are filled with the same one from the followed array.
 * f([[a,b,c],[1,null,3]) = "abc\n1b3".
 * @param {type} all 
 * @param {type} len length of the larged 2D array
 * @param {type} glueA sticks values of arrayOfArrays
 * @param {type} glueB sticks values of 2D arrays.
 * @returns {type}
 */

function utworzElementy(all, len = null, glueA = "\r\n", glueB = ", ") {
    if (len === null) {
        len = all[0].length;
    }
    let mem = new Array(len);
    let out = "\n";
    let m = 0;
    for (var a in all) {
        let tmp = "";

        for (var b in all[a]) {
            out += all[a][b] !== null ? (mem[m] = all[a][b]) : mem[m];
            m++;
            b++;
        }
        out += glueA;
        m = 0;
    }
    return out;
}




/**
 * 
 * @param {type} target
 * @param {type} tags
 * @param {type} attributes
 * @param {type} inners
 * @returns {undefined}
 */
function multiElement(target, tags, attributes, inners) {
rerurn;
}
/**
 * Usuwa cel, jeżeli: (event OR cel)==null OR event.target.id==cel
 * 
 * @param {type} event 
 * @param {type} cel
 * @returns {undefined}
 */
function usuńPopup(event = null, cel = null) {
    if (!(event && cel) || (event.target.id == cel)) {
        document.getElementById(cel).innerHTML = '';
        document.getElementById(cel).style.display = "none";
    }
}

