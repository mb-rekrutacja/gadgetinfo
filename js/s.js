/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Funkcja zmienia regułę css tak, aby listujywać żądane klasy.
 * Np klasa .class będzie listujana dopiero, gdy ".class .show."
 * lub klasa .class będzie listujana dopeiro, gdy nie  będzie ".class .hide"
 * Ewentualnie przemienia (toggle) atrybut "widoczny=true|false" 
 * 
 * @param {Array|string} listuj Klasy listujane 
 * @param {Array|string} schowaj Klasy chowane 
 * 
*/
function listuj(listuj,schowaj='p',obszar=''){
    listuj= Array.isArray(listuj)?listuj:[listuj];
    schowaj=Array.isArray(schowaj)?schowaj:[schowaj]

    // schowaj.map(function(el) {
        // document.querySelectorAll(obszar+" "+el).map((e)=>{
            // e.classList.indexOf("ukryj") === -1 && e.classList.push("ukryj");
        // });
    // });
    listuj.map(function(el) {
        document.querySelectorAll(obszar+" "+el).map((e)=>{
            e.classList.indexOf("listuj") === -1 && e.classList.push("listuj");
        });
    });

}
function scroll(zwrot) { // We need to wrap the loop into an async function for this to work

	document.querySelector(".roller-wrap").scrollLeft += zwrot * document.querySelector(".strona").offsetWidth;

}
//document.querySelector("[type='search']").addEventListener(("select"), (e) => e.target.value);
document.querySelector(".roller_lewo").addEventListener(("click"), () => scroll(-1));
document.querySelector(".roller_prawo").addEventListener(("click"), () => scroll(1));
document.querySelector("#wszystko").addEventListener("change", (e) => {
        let id="#"+e.target.value.replaceAll(" ","");
	location.href = id;
	let url = location.href;               //Save down the URL without hash.
        document.querySelector(id).setAttribute("open","");
        document.querySelector(id).addEventListener("focusout",()=>{
                    document.querySelector(id).removeAttribute("open");

        });
	history.replaceState(null, null, url);
        document.querySelector("[type='search']").value=null;
});